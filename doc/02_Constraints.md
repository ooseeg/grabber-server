[zurück](01_Intro.md) [weiter](03_Context.md)
#Randbedingungen

##organisatorisch
- HW muss wenig Kosten
- HW-Anpassung von einem Entwickler 
- Erst-Entwicklung muss schnell umgesetzt sein (wenig Zeit, wenig Entwickler)
- Erst-Entwicklung mit einem Entwickler
- Nach-Entwicklung muss mit 4-8 TN an einem Tag (größtenteils) möglich sein

##technisch
- Server-HW: raspi 3 oder schwächer (Alternative: arduino)
- Client-HW: Android oder iOS-Handy



[zurück](04_Approach.md) [weiter](06_Runtime.md)
#Bausteinsicht

##1. Überblick

![Bausteine](img/building_blocks_lvl1.png?raw=true "Bausteinsicht")

###1 Steuerung
Steuert den Automaten. Verwaltet Spiel-Ablauf

###2 Verwaltung
Bietet dem Admin die Möglichkeit, den Automaten nach einem Fehler in einen gültigen Zustand zu versetzen.

###3 Füllstand-Überwachung
Zählt wie viel dem Automaten entnommen wird und benachrichtigt ggf. VM, damit der Automat wieder befüllt wird

##2. Details

###2.1 Steuerung

![Steuerung](img/building_blocks_lvl2_steuerung.png?raw=true "Steuerung")

###2.1.1 Benutzoberfläche
* mobile Webseite
* liest Orientierung des Smartphones
* sendet Steuer-Requests an Steuerungsserver

###2.1.2 Steuerungsserver
* node.js-App
* bietet HTTP-Interface
* übersetzt Requests nach /move/(right|left|down|up) oder /grab in I2C-Messages

###2.1.3 Motorsteuerung
* Motor-Hat von adafruit 
* Relais
[zurück](02_Constraints.md) [weiter](04_Approach.md)
#Kontext

![Kontext-Skizze](img/context.png?raw=true "Systemkontext")

##logisch
1. Spieler
* Besucher im oose.campus oder auf Messe
* Möchte mit seinem Handy den Greifarm steuern, um Süßes aus dem Automaten zu holen

2. Admin
* oose-Mitarbeiter
* verwaltet die Anwendung: startet, stoppt, behebt Fehler

3. VM
* befüllt den Automaten

4. Süßigkeitenautomat
* die Motoren des Automaten müssen gesteuert werden, damit Spieler an ihren Süßkram kommen

##technisch
1. Orientierungssensoren des Handys
Orientierungsdaten werden ausgelesen und an das System übertragen, welches diese in Steuerbefehle für den den Grabber umsetzt.
Zugriff via Web-Client

2. Servomotoren für x-&y-Bewegung des Greifers
Steuerung durch System via PWM über GPIOs

3. Servomotor für "Nach-Unten-& Greifen" des Greifers
Steuerung durch System via PWM(?) über GPIOs



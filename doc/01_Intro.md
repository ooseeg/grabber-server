[weiter](02_Constraints.md)
#Intro

##Vision
Candy-Grabber mit dem Handy steuern: eine spaßige Demo-Anwendung für 1-Tages-Workshops und Konferenzen

##Qualitätsziele
* Benutzbarkeit
    * verständlich
    * attraktiv
    * erlernbar
    * bedienbar
* Zuverlässigkeit
    * robust
    * fehlertolerant
    * wiederherstellbar
    * reif
* Sicherheit
    * integer
* Performanz
    * gutes Zeitverhalten



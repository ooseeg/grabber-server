[zurück](03_Context.md) [weiter](05_Building-Blocks.md)
#Lösungsansätze

1. **mobiler Web-Client**: kostet nichts, leicht zu entwickeln
2. **node.js**-Server mit express: leichtgewichtig, schnell, gpio- & i2c-libs für Raspberry Pi vorhanden, interessant, Know-How für SWA-Web brauchbar



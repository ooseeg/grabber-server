#!/usr/bin/env node
var app = require('../app');
const https = require('https');
const fs = require('fs');

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), '0.0.0.0',function () {
  console.log('Express server listening on port ' + server.address().port);
});

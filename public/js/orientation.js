var lo = -10;
var hi = 10;
var lastTimestamp = 0;
var intervalBetweenRequests = 400;
var lastCall = "";

function handleOrientation(event) {
    var pitch = event.beta.toFixed(1);       // links-rechts in Grad zwischen -180 bis 180
    var roll = event.gamma.toFixed(1);       // hoch-runter in Grad zwischen -90 bis 90
    if (window.innerHeight < window.innerWidth) {
        hideLandscapeWarning();
        visualize(pitch, roll);
        move(pitch, roll);
    } else {
        showLandscapeWarning();
        stop();
    }
}

function hideLandscapeWarning() {
    document.getElementById("warning").style.display = "none";
}

function showLandscapeWarning() {
    document.getElementById("warning").style.display = "block";
}

function visualize(pitch, roll) {
    document.getElementById("status").innerHTML = "pitch: " + pitch + "<br> roll: " + roll;
    document.getElementById("right").className = pitch > hi ? "box active" : "box";
    document.getElementById("left").className = pitch < lo ? "box active" : "box";
    document.getElementById("top").className = roll > hi ? "box active" : "box";
    document.getElementById("bottom").className = roll < lo ? "box active" : "box";
}

function move(pitch, roll) {
    var now = Date.now();
    if (lastTimestamp + intervalBetweenRequests < now) {
        lastTimestamp = now;
        if (pitch > hi && lastCall !== "right") {
            lastCall = "right";
            put('/move/right', {});
        } else if (pitch < lo && lastCall !== "left") {
            lastCall = "left";
            put('/move/left', {});
        } else if (roll > hi && lastCall !== "back") {
            lastCall = "back";
            put('/move/back', {});
        } else if (roll < lo && lastCall !== "front") {
            lastCall = "front";
            put('/move/front', {});
        } else if ((pitch < hi && pitch > lo) && (roll < hi && roll > lo) && lastCall !== "stop") {
            stop(pitch, roll);
        }
    }
}

function stop() {
    lastCall = "stop";
    put('/move/not', {});
}

function put(uri, data) {
    return $.ajax({
        url: uri,
        type: 'PUT',
        error: function (jqXHR) {
            console.log("ajax error -> " + jqXHR.status)
        }
    });
}

window.addEventListener("deviceorientation", handleOrientation, true);

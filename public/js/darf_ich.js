if (window.DeviceOrientationEvent) {
    window.addEventListener("deviceorientation", orientation, false);
    document.getElementById("status").innerHTML = "<a href='candy'> Grab your Candy :-)</a>";
}
else {
    document.getElementById("status").innerHTML = "DeviceOrientationEvent is not supported <br>";
}

function orientation(event) {
    console.log("Magnetometer: "
        + event.alpha + ", "
        + event.beta + ", "
        + event.gamma
    );
}


var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    res.render('index', {title: 'Express'});
});

router.get('/candy', function (req, res) {
    res.render('candy');
});

module.exports = router;

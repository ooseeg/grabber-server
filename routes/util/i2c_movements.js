var i2c = require('i2c');
var wire = new i2c(0x60, {device: '/dev/i2c-1'});
var gpio = require('gpio');
var sleep = require('sleep');

var gpio10 = gpio.export(10, {
    ready: function () {
        console.log("GPIO 10 ready")
    }
});

var gpio22 = gpio.export(22, {
    ready: function () {
        console.log("GPIO 22 ready")
    }
});

function down() {
    wire.write([0x15, 0x10], log);
    wire.write([0x17, 0x10], log);
    wire.write([0x11, 0x0F], log);
}

function up() {
    wire.write([0x13, 0x10], log);
    wire.write([0x19, 0x10], log);
    wire.write([0x11, 0x0F], log);
}

exports.grab = function () {
    down();
    sleep.msleep(2300);
    this.stop();
    up();
    sleep.msleep(2200);
    this.stop();
};

exports.right = function () {
    gpio10.set(1);
    wire.write([0x2B, 0x10], log);
    wire.write([0x31, 0x10], log);
    wire.write([0x29, 0x0F], log);
};

exports.left = function () {
    gpio10.set(0);
    wire.write([0x2D, 0x10], log);
    wire.write([0x2F, 0x10], log);
    wire.write([0x29, 0x0F], log);
};

exports.front = function () {
    gpio22.set(1);
    wire.write([0x1F, 0x10], log);
    wire.write([0x1D, 0x10], log);
    wire.write([0x25, 0x0F], log);
};

exports.back = function () {
    gpio22.set(0);
    wire.write([0x21, 0x10], log);
    wire.write([0x1B, 0x10], log);
    wire.write([0x25, 0x0F], log);
};

//WTF?
exports.stop = function () {
    wire.write([0x29, 0x00], log);

    wire.write([0x2B, 0x00], log);
    wire.write([0x2D, 0x00], log);
    wire.write([0x2F, 0x00], log);
    wire.write([0x31, 0x00], log);

    wire.write([0x1A, 0x00], log);
    wire.write([0x1B, 0x00], log);
    wire.write([0x1D, 0x00], log);
    wire.write([0x20, 0x00], log);
    wire.write([0x1F, 0x00], log);
    wire.write([0x21, 0x00], log);
    wire.write([0x24, 0x00], log);

    wire.write([0x11, 0x00], log);
    wire.write([0x13, 0x00], log);
    wire.write([0x15, 0x00], log);
    wire.write([0x17, 0x00], log);
    wire.write([0x19, 0x00], log);
};

function log(message) {
    if (message !== null && message !== undefined) {
        console.log(message);
    }
}

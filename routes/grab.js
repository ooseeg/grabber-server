var express = require('express');
var router = express.Router();
var move = require('./util/i2c_movements');

router.put('/', function (req, res) {
    move.grab();
    res.status(204).end();
});

module.exports = router;
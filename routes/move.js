var express = require('express');
var router = express.Router();
var move = require('./util/i2c_movements');

router.put('/right', function (req, res) {
    move.right();
    res.status(204).end();
});

router.put('/left', function (req, res) {
    move.left();
    res.status(204).end();
});

router.put('/back', function (req, res) {
    move.back();
    res.status(204).end();
});

router.put('/front', function (req, res) {
    move.front();
    res.status(204).end();
});

router.put('/not', function (req, res) {
    move.stop();
    res.status(204).end();
});

module.exports = router;
